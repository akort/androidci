Creating the project:
https://mycourses.aalto.fi/mod/page/view.php?id=457640

Deploying nodejs app to google cloud:
https://cloud.google.com/nodejs/getting-started/tutorial-app

Authenticating to firestore:
https://googleapis.dev/nodejs/firestore/latest/index.html

- Get credentials for a service user
- Save as env variable, add to .bashrc so you don't have to set it up again
- Run 'node firestore_test.js to test authentication'