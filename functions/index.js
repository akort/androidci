'use strict';

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//

// Import requirements
const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp()
const spawn = require('child-process-promise').spawn;
const path = require('path');
const os = require('os');
const fs = require('fs');
const bucketName = 'mcc-g23-bucket-test'

//Cloud function for generating different resolution images when an image is uploaded to cloud
exports.generateResolutions = functions.region('europe-west1').storage.bucket(bucketName).object().onFinalize(async (object) => {

    // Define some constants
    const fileBucket = object.bucket; // The Storage bucket that contains the file.
    const filePath = object.name; // File path in the bucket.
    const contentType = object.contentType; // File content type.
    const metageneration = object.metageneration; // Number of times metadata has been generated. New objects have a value of 1.


    // Exit if this is triggered on a file that is not an image.
    if (!contentType.startsWith('image/')) {
        return console.log('This is not an image.');
    }

    // Exit if the uploaded image is already processed
    const fileName = path.basename(filePath);
    console.log('Checking image: ', fileName)

    if (fileName.startsWith('low_') || fileName.startsWith('high_') || fileName.startsWith('full_')) {
        return console.log('This is an already processed image.');
    }

    // Download file from bucket.
    const bucket = admin.storage().bucket(fileBucket);
    const tempFilePath = path.join(os.tmpdir(), fileName);
    const metadata = {
        contentType: contentType,
    };
    await bucket.file(filePath).download({destination: tempFilePath});
    console.log('Image downloaded locally to', tempFilePath);
    
    // Generate a low resolution version using ImageMagick.
    await spawn('convert', [tempFilePath, '-thumbnail', '200x200>', tempFilePath]);
    console.log('Low resolution version created at', tempFilePath);

    // We add a 'low_' prefix to file name. That's where we'll upload the thumbnail.
    const lowFileName = `low_${fileName}`;
    const lowFilePath = path.join(path.dirname(filePath), lowFileName);
    
    // Uploading the thumbnail.
    await bucket.upload(tempFilePath, {
        destination: lowFilePath,
        metadata: metadata,
    });
    
    // Once the thumbnail has been uploaded delete the local file to free up disk space.
    return fs.unlinkSync(tempFilePath);

});

// A hello world function
exports.helloWorld = functions.https.onRequest((request, response) => {
    response.send("Hello from Firebase!");
});
