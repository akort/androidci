package com.example.taskmanager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity {

    EditText usernameField;
    EditText passwordField;
    Button signInButton;
    TextView registrationLink;

    String address = "https://google.fi";
    //"https://mcc-fall-2019-g23.appspot.com/";

    final int RC_SIGN_IN = 123;
    //List<AuthUI.IdpConfig> providers = Arrays.asList(new AuthUI.IdpConfig.EmailBuilder().build());

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /* Set up the UI elements */
        usernameField = findViewById(R.id.usernameField);
        passwordField = findViewById(R.id.passwordField);
        signInButton = findViewById(R.id.signInButton);
        registrationLink = findViewById(R.id.registerLink);

        mAuth = FirebaseAuth.getInstance();

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String emailAddr = usernameField.getText().toString();
                String password = passwordField.getText().toString();

                if (emailAddr.equals("") || password.equals("") || emailAddr == null || password == null) {
                    Toast.makeText(getApplicationContext(), "Please fill in both email and password!", Toast.LENGTH_SHORT)
                            .show();

                } else {

                /* Make an authentication request*/
                mAuth.signInWithEmailAndPassword(emailAddr, password)
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
                                    startActivity(intent);
                                    finish(); // disable navigating back to login when successfully logged in
                                } else {
                                    Toast.makeText(getApplicationContext(), "Login failed", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                }
            }
        };
        signInButton.setOnClickListener(listener);

        registrationLink.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), RegistrationActivity.class);
                    startActivity(intent);
                }
        }
        );

    }

    /** Processes the result when coming from some other activity */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        // If the request was successful and came from the sign in activity, report success to user
        if(resultCode == Activity.RESULT_OK && requestCode == RC_SIGN_IN) {
            Toast.makeText(getApplicationContext(), "Signed in", Toast.LENGTH_SHORT).show();
            Intent i = new Intent(getApplicationContext(), ProfileActivity.class);
            startActivity(i);
            finish();
        }
        else {
            Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mAuth.getCurrentUser() != null) {
            Intent intent = new Intent(this, ProfileActivity.class);
            startActivity(intent);
            finish(); // disable navigating back to login when successfully logged in
        }
    }
}

