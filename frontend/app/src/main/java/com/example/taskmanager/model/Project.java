package com.example.taskmanager.model;

import org.json.*;

public class Project {
    private String title;
    private String type;

    public Project(String title, String type) {
        this.title = title;
        this.type = type;
    }

    public String getTitle() {
        return this.title;
    }

    public String getType() {
        return this.type;
    }

    /**
     *  Creates and returns a Project instance based on the given JSON string
     * @param json  A JSON string representing a project
     * @return  Project instance initialized with the values provided in the JSON string
     * @throws JSONException
     */
    static Project fromJson(String json) throws JSONException {
        JSONObject obj = new JSONObject(json);
        String title = obj.getString("title");
        String type = obj.getString("type");

        return new Project(title, type);
    }

}
