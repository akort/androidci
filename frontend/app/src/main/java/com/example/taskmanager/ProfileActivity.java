package com.example.taskmanager;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.util.Log;

public class ProfileActivity extends FragmentActivity {

    private ViewPager pager;
    private FragmentStatePagerAdapter adapter;
    private int NUMBER_OF_PAGES = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        pager = findViewById(R.id.menuPager);
        adapter = new FragmentStatePagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                Log.i("MCC", "Position :" + position);
                String desc = "";
                switch(position) {
                    case 0: desc = "Projects";
                        break;
                    case 1: desc =  "Tasks";
                        break;
                    case 2: desc = "Settings";
                        break;
                    default: desc = "Unknown fragment";
                        break;
                }

                return ListFragment.newInstance(desc);
            }

            @Override
            public int getCount() {
                return NUMBER_OF_PAGES;
            }
        };



        pager.setAdapter(adapter);

    }
}
