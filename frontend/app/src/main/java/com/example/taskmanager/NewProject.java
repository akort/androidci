package com.example.taskmanager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.text.FirebaseVisionText;
import com.google.firebase.ml.vision.text.FirebaseVisionTextRecognizer;

import org.json.JSONObject;
import com.example.taskmanager.api.APIClient;


public class NewProject extends AppCompatActivity {

    // UI elements
    EditText name, desc, date, keywords;
    ImageView projectImage;
    Button addProject;

    final int CAMERA_REQUEST_CODE = 0;
    final int GALLERY_REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_project);

        /* Set up references to UI elements */
        name = findViewById(R.id.newProjectName);
        desc = findViewById(R.id.newProjectDescription);
        date = findViewById(R.id.newProjectDeadline);
        keywords = findViewById(R.id.newProjectKeywords);

        projectImage = findViewById(R.id.newProjectImage);
        addProject = findViewById(R.id.newProjectAdd);

        /* Give the user a chance to give the project an icon from gallery or camera */
        projectImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] options = {"Camera", "Gallery", "Cancel"};
                AlertDialog.Builder dialogBuilder  = new AlertDialog.Builder(projectImage.getContext());
                dialogBuilder.setTitle("Choose image source");
                dialogBuilder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch(which) {
                            case 0:
                                //Toast.makeText(getApplicationContext(), "Camera", Toast.LENGTH_SHORT).show();
                                if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
                                {
                                    requestPermissions(new String[]{Manifest.permission.CAMERA}, 100);
                                }
                                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(cameraIntent, CAMERA_REQUEST_CODE);

                                break;
                            case 1:
                               // Toast.makeText(getApplicationContext(), "Gallery", Toast.LENGTH_SHORT).show();
                                Intent galleryIntent = new Intent(Intent.ACTION_PICK);
                                galleryIntent.setType("image/*");
                                startActivityForResult(galleryIntent, GALLERY_REQUEST_CODE);
                                break;
                        }

                    }
                });
                dialogBuilder.show();
            }
        });
        addProject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    JSONObject requestBody = new JSONObject();
                    requestBody.put("name", name.getText().toString());
                    requestBody.put("description", desc.getText().toString());
                    requestBody.put("type", "PERSONAL");
                    APIClient.post("http://10.0.2.2:3000/project", "POST", requestBody);
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("MCC", "Could not make API call");
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        Log.i("MCC", "Result code: " + resultCode);
        if(resultCode == RESULT_OK) {
            if(requestCode == CAMERA_REQUEST_CODE) {
                Log.i("MCC", "CAMERA!!!");
                projectImage.setImageBitmap((Bitmap) data.getExtras().get("data"));
            }
            else if(requestCode == GALLERY_REQUEST_CODE){
                Log.i("MCC", "GALLERY");
                projectImage.setImageURI(data.getData());
                recognizeText();
            }
        }
    }


    // Recognizes text in an image
    private void recognizeText() {
        Bitmap bmp = ((BitmapDrawable) projectImage.getDrawable()).getBitmap();
        FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(bmp);
        FirebaseVisionTextRecognizer recognizer = FirebaseVision.getInstance().getOnDeviceTextRecognizer();
        Task<FirebaseVisionText> textTask = recognizer.processImage(image).
                addOnSuccessListener(new OnSuccessListener<FirebaseVisionText>() {
            @Override
            public void onSuccess(FirebaseVisionText firebaseVisionText) {
                Toast.makeText(getApplicationContext(), firebaseVisionText.getText(), Toast.LENGTH_SHORT).show();
                for(FirebaseVisionText.TextBlock block: firebaseVisionText.getTextBlocks()){
                    Log.i("MCC", "Block: " + block.getText());
                }


            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });

    }

}
