package com.example.taskmanager.api;

import android.os.AsyncTask;
import android.util.Log;

import androidx.annotation.NonNull;

import org.json.JSONObject;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GetTokenResult;

import java.util.concurrent.ExecutionException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class APIClient {
    public static void post(final String url, final String httpMethod, final JSONObject body) {
        FirebaseAuth
                .getInstance()
                .getCurrentUser()
                .getIdToken(true)
                .addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
                    @Override
                    public void onComplete(@NonNull Task<GetTokenResult> task) {
                        if (task.isSuccessful()) {
                            String token = task.getResult().getToken();
                            try {
                                RequestTask requestTask = new RequestTask("https://mcc-fall-2019-g23-259513.appspot.com/project", "POST", body, token);
                                String result = requestTask.execute().get();
                                System.out.println(result);
                                Log.i("MCC API", result);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    }
                });

    }
}

class RequestTask extends AsyncTask<String, Void, String> {

    private String urlAddr, httpMethod, token;
    private JSONObject bodyObject;

    public RequestTask(String urlAddr, String httpMethod, JSONObject body, String token) {
        this.urlAddr = urlAddr;
        this.httpMethod = httpMethod;
        this.token = token;
        this.bodyObject = body;
    }


    @Override
    protected String doInBackground(String... strings) {
        try {

            String requestBody = bodyObject.toString();

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url("https://mcc-fall-2019-g23-259513.appspot.com/project")
                    .addHeader("Authorization", token)
                    .post(RequestBody.create(MediaType.get("application/json"), requestBody))
                    .build();

            Response response = client.newCall(request).execute();
            int status = response.code();
            Log.i("MCC Status", Integer.toString(status));
            String responseContent = response.body().string();
            return responseContent;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
    }
}
