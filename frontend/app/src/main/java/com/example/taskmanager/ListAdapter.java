package com.example.taskmanager;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.firebase.firestore.DocumentSnapshot;


/** Required to render each list item layout with the information in each list item*/
public class ListAdapter extends ArrayAdapter<DocumentSnapshot> {
    private DocumentSnapshot[] docs;

    public ListAdapter(Context ctx, DocumentSnapshot[] documents) {
        super(ctx, R.layout.list_item, documents);
        this.docs = documents;
    }


    /** Does the actual rendering of each list item*/
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item, parent, false);

        DocumentSnapshot doc = this.docs[position];

        TextView tw  = convertView.findViewById(R.id.taskName);
        tw.setText(doc.getString("name"));
        TextView tw2 = convertView.findViewById(R.id.role);
        tw2.setText(doc.getString("type"));

        return convertView;
    }

}

