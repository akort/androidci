package com.example.taskmanager.api;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;


import javax.net.ssl.HttpsURLConnection;

/**
 *  Represents an API call to the backend.
 *  Performs the call on a background thread thus keeping the UI responsive
 */
public class APICall extends AsyncTask<String, Void, String> {

    private String address, method, data;
    private Context ctx; // Context for Toast
    private boolean success = false;

    /**
     *
     * @param address The URL address to be queried
     * @param method The HTTP method to be used, e.g. GET or POST
     * @param data The data to be sent with the query
     */
    public APICall(Context ctx , String address,  String method, String data) {
        this.address = address;
        this.method = method;
        this.data = data;
        this.ctx = ctx;

    }



    @Override
    protected String doInBackground(String... strings) {
        String res = "";
        try {

            URL url = new URL(address);
            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
            conn.setRequestMethod(method);
            InputStream is = conn.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));

            String line;
            while((line = reader.readLine()) != null){
                res += line + "\n";
            }
            success = true;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return res;
    }

    public void onPostExecute(String res ){
        String report = success ? "Done" : "A problem occured connecting to API";
        Toast.makeText(ctx, report, Toast.LENGTH_SHORT).show();
        Log.i("MCC", res);

    }
}
