package com.example.taskmanager;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;


public class ListFragment extends Fragment {

    private TextView fragmentText;
    private Button newProjectButton, signOutButton;
    private ListView listView;
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    /** A helper function for giving arguments to a ListFragment*/
    public static ListFragment newInstance(String txt) {
        Bundle b = new Bundle();
        b.putString("text", txt);

        ListFragment f = new ListFragment();
        f.setArguments(b);

        return f;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_list, container, false);

        fragmentText = rootView.findViewById(R.id.fragment_text);
        listView = rootView.findViewById(R.id.listView);

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        // Fetch project data.
        this.getProjectsForUser();

        String txt = getArguments().getString("text");
        fragmentText.setText(txt);

        /* Give the user a chance to add a new project */
        newProjectButton = rootView.findViewById(R.id.newProjectButton);
        newProjectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity().getApplicationContext(), NewProject.class);
                startActivity(i);
            }
        });

        /* Sign out functionality using Firebase libraries*/
        signOutButton = rootView.findViewById(R.id.signOutButton);
        signOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAuth.signOut();
                Intent intent = new Intent(getContext(), MainActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        this.getProjectsForUser();
    }

    private void getProjectsForUser() {
        String userId = mAuth.getCurrentUser().getUid();
        db.collection("projects")
                .whereArrayContains("members", userId)
                .get()
                .addOnCompleteListener(
                        new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                if (task.isSuccessful()) {
                                    DocumentSnapshot[] docs = task.getResult().getDocuments().toArray(new DocumentSnapshot[0]);
                                    listView.setAdapter(new ListAdapter(getActivity(), docs));
                                } else {
                                    Log.i("MCC - Projects", "No projects found");
                                }
                            }
                        }
                );
    }
}




