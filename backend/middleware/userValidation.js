const Firebase = require('../firebase-admin');

// Checks user's ID token against Firebase authentication
exports.validateUser = async (req, res, next) => {
    const authToken = req.get('Authorization');
    try {
        const token = await Firebase.auth().verifyIdToken(authToken);
        if (token) {
            req.user = token.uid;
            return next();
        } else {
            res.status(401).json({ message: 'Unauthorized request' });
        }
    } catch (err) {
        console.log(err);
        if (err.errorInfo && err.errorInfo.code === 'auth/argument-error') {
            return res.status(400).end();
        }
        res.status(500).end();
    }
};