const admin = require('firebase-admin');
//const serviceAccount = require('./mcc-fall-2019-g23-259513-firebase-adminsdk-n2qzy-b88bf45531.json');
let app;

exports.initializeApp = () => {
    app = admin.initializeApp({
        credential: admin.credential.applicationDefault(),
        databaseURL: "https://mcc-fall-2019-g23-259513.firebaseio.com"
});
};

exports.auth = admin.auth;

exports.firestore = admin.firestore