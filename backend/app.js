'use strict';

const express = require('express');
const bodyParser = require('body-parser');

const Firebase = require('./firebase-admin');
const { validateUser } = require('./middleware/userValidation');

const { createProject, addMembers, deleteProject } = require('./controller/project');
const { createTask, assignTask, updateTask } = require('./controller/task');

const app = express();

Firebase.initializeApp();

app.set('case sensitive routing', true);
app.use(bodyParser.json());

// Validate Firebase ID token provided by client.
app.use('/', validateUser);


// Routes for project
app.post('/project', createProject);
app.put('/project/:projectId/:userId', addMembers);
app.delete('/projects/:projectId', deleteProject);


// Routes for task
app.post('/task', createTask);
app.put('/tasks/:taskId/', updateTask);
app.put('/tasks/:taskId/:assigneeId', assignTask);


const PORT = process.env.PORT || 3000;

app.listen(PORT, () => {
	console.log(`App listening on port ${PORT}`);
	console.log('Press Ctrl+C to quit.');
});

