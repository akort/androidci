/**
 *     This file contains controllers related to projects. 
 */

// Set up Firebase Admin SDK credentials
const { firestore } = require('../firebase-admin');

exports.createProject = async (req, res) => {
    try {
        const { name, description, type, deadline } = req.body;
        
        // Request is invalid if these fields are missing => 400 Bad Request.
        if (!name || !description || !type) {
          return res.status(400).end();
        }
        const uid = req.user;
        const createdAt = new Date(); // timestamp for project creation

        const dataoObject = {
          name,
          description,
          type,
          createdAt,
          administrator: uid,
          deadline: deadline ? new Date(deadline) : null,
          members: [uid]
        };


        const document = await firestore().collection('projects').add(dataoObject);

        res.status(201).json({ projectId: document.id }); // static dummy IDs
    } catch (err) {
        console.log(err);
        // Internal Server Error
        res.status(500).end();
    }
    
};

exports.deleteProject = async (req, res) => {
    try {
      const { projectId } = req.params;
      if (!projectId) {
        res.status(400).end();
      } else {
        await firestore().collection('projects').doc(projectId).delete();
        res.status(200).json({ message: 'Project was deleted successfully', id: projectId });
      }
    } catch (err) {
      console.log(err);
      res.status(500).end();
    }
};

exports.addMembers = async (req, res) => {
  try {
    const { userId, projectId } = req.params;
    
    if (!userId || !projectId) {
      return res.status(400).end();
    }

    await firestore()
      .collection('projects')
      .doc(projectId)
      .update('members', firestore.FieldValue.arrayUnion(userId));
    res.status(200).json({ message: 'Member(s) added successfully' });

  } catch (err) {
    console.log(err);
    res.status(500).end();
  }
};