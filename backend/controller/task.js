/**
 * This file contains controllers related to tasks.
 * 
 */


const { firestore } = require('../firebase-admin');

const PENDING = 'PENDING';
const ONGOING = 'ONGOING';
const COMPLETED = 'COMPLETED';

const isStatusValid = (status) => [PENDING, ONGOING, COMPLETED].includes(status);

exports.createTask = async (req, res) => {
    try {
        const { projectId, description, status, deadline } = req.body;
        
        if ( !projectId || !description || !status || !deadline) {
            return res.status(400).end();
        } else if (!isStatusValid(status)) {
            return res.status(400).end();
        }
        
        const document = await firestore().collection('tasks').add({
            project: projectId,
            description,
            status,
            deadline: new Date(deadline),
            assignees: []
        });
        res.status(201).json({ taskId: document.id });
       
    } catch (err) {
        console.log(err);
        res.status(500).end();
    }

};

exports.updateTask = async (req, res) => {
    try {
        const { description, status, deadline, assignees } = req.body;
        const { taskId } = req.params;
        
        if (!status || !isStatusValid(status)) {
            return res.status(400).end();
        }

        await firestore().doc(`tasks/${taskId}`).update({
            description,
            status,
            deadline,
            assignees
        });

        res.status(200).json({ message: 'Task updated successfully', id: taskId });
    } catch (err) {
        res.status(500).end();
    }
};

exports.assignTask = async (req, res) => {
    try {
        const { taskId, assigneeId } = req.params;

        if (!taskId) {
            res.status(400).end();
        }
        await firestore()
            .collection('tasks')
            .doc(taskId)
            .update('assignees', firestore.FieldValue.arrayUnion(assigneeId));
        res.status(200).json({ message: 'Task was assigned successfully', id: taskId });
    } catch (err) {
        console.log(err);
        res.status(500).end();
    }
};